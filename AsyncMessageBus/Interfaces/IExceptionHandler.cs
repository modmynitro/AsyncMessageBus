﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsyncMessageBus.Interfaces
{
    public interface IExceptionHandler
    {
        void Handle(Exception exception);
    }
}
