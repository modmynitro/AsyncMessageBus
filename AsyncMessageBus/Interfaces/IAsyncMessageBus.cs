﻿using System;
using System.Threading.Tasks;

namespace AsyncMessageBus.Interfaces
{
    public interface IAsyncMessageBus
    {
        /// <summary>
        /// Posts the message and returns immediate.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="message">The message.</param>
        void PostMessage<TMessage>(TMessage message)
            where TMessage : IMessage;

        /// <summary>
        /// Sends the message and waits for all messages to be processed.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="message">The message.</param>
        /// <returns>A task that represents the asynchronous operation. Never <see langword="null"/></returns>
        Task SendMessage<TMessage>(TMessage message)
            where TMessage : IMessage;

        /// <summary>
        /// Subscribes to the specified message.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="receiver">The receiver. Should not be <see langword="null"/></param>
        /// <param name="func">The function.</param>
        void Subscribe<TMessage>(object receiver, Func<TMessage, Task> callback)
            where TMessage : IMessage;
    }
}