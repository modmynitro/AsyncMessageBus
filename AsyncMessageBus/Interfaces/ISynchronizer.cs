﻿using System;
using System.Threading.Tasks;

namespace AsyncMessageBus.Interfaces
{
    public interface ISynchronizer
    {
        /// <summary>
        /// Gets a value indicating whether the caller must call an synchronization method when
        /// making method calls.
        /// </summary>
        /// <value><see langword="true"/> if synchronization is required; otherwise, <see langword="false"/>.</value>
        bool SynchronizationRequired { get; }

        /// <summary>
        /// Synchronizes the synchronous execution of the specified <see cref="Action"/>.
        /// </summary>
        /// <param name="callback">A delegate to invoke through the dispatcher. Must not be <see langword="null"/>.</param>
        void Synchronize(Action callback);

        /// <summary>
        /// Synchronizes the asynchronous execution of the specified <see cref="Func{TResult}"/>.
        /// </summary>
        /// <param name="callback">The callback. Must not be <see langword="null"/></param>
        /// <returns>A task that represents the asynchronous operation. Never <see langword="null"/></returns>
        Task SynchronizeAsync(Func<Task> callback);
    }
}
