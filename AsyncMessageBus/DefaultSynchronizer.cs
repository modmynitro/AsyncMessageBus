﻿using System;
using System.Threading.Tasks;

using AsyncMessageBus.Interfaces;

namespace AsyncMessageBus
{
    internal class DefaultSynchronizer : ISynchronizer
    {
        private DefaultSynchronizer()
        {
        }

        public static ISynchronizer Synchronizer { get; } = new DefaultSynchronizer();

        public bool SynchronizationRequired => false;

        public void Synchronize(Action callback)
        {
            throw new NotImplementedException();
        }

        public Task SynchronizeAsync(Func<Task> callback)
        {
            throw new NotImplementedException();
        }
    }
}