﻿namespace AsyncMessageBus.Enums
{
    public enum RequestedAction
    {
        SendMessage,
        PostMessage,
        Subscribe,
        Unsubsribe
    }
}
