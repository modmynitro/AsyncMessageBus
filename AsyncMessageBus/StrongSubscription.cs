﻿using System;
using System.Threading.Tasks;

using AsyncMessageBus.Interfaces;

namespace AsyncMessageBus
{
    internal class StrongSubscription<TMessage> :
        ISubscription
        where TMessage : IMessage
    {
        private readonly object _target;
        private readonly Func<TMessage, Task> _callback;

        public StrongSubscription(object target, Func<TMessage, Task> callback)
        {
            _target = target;
            _callback = callback;
        }

        public bool IsAlive => true;
        public Type MessageType => typeof(TMessage);

        public bool Equals(ISubscription other)
        {
            if (other == null)
                return false;
            if (ReferenceEquals(this, other))
                return true;

            return MessageType == other.MessageType && other.HasReceiver(_target);
        }
            
        public bool HasReceiver(object receiver) => _target == receiver;

        public Task SendMessage(IMessage message) => _callback((TMessage)message);
    }
}
