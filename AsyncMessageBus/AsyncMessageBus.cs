﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AsyncMessageBus.Interfaces;

namespace AsyncMessageBus
{
    internal class AsyncMessageBus : IAsyncMessageBus
    {
        private readonly SynchronizationHelper _synchronizer;
        private readonly List<ISubscription> _subscriptions = new List<ISubscription>();
        private readonly IExceptionHandler _exceptionHandler;
        private readonly object _lock = new object();
        private readonly ConcurrentQueue<(bool Remove, ISubscription Subscription)> _queue = new ConcurrentQueue<(bool Remove, ISubscription Subscription)>();

        public AsyncMessageBus(ISynchronizer? synchronizer, IExceptionHandler? exceptionHandler)
        {
            _synchronizer = new SynchronizationHelper(synchronizer ?? DefaultSynchronizer.Synchronizer);
            _exceptionHandler = exceptionHandler ?? DefaultExceptionHandler.ExceptionHandler;
        }

        public void PostMessage<TMessage>(TMessage message)
            where TMessage : IMessage
        {
            if (message is null)
                throw new ArgumentNullException(nameof(message));

            Task.Run(async () => {
                try
                {
                    await this.SendMessageInternal(message);
                }
#pragma warning disable CA1031 // Do not catch general exception types
                catch (Exception exception)
                {
                    _exceptionHandler.Handle(exception);
                }
#pragma warning restore CA1031 // Do not catch general exception types
            }).ConfigureAwait(false);
        }

        public Task SendMessage<TMessage>(TMessage message)
            where TMessage : IMessage
        {
            if (message is null)
                throw new ArgumentNullException(nameof(message));

            return _synchronizer.Synchronize(() => this.SendMessageInternal(message));
        }

        public void Subscribe<TMessage>(object receiver, Func<TMessage, Task> callback)
            where TMessage : IMessage
        {
            if (receiver is null)
                throw new ArgumentNullException(nameof(receiver));

            if (callback is null)
                throw new ArgumentNullException(nameof(callback));

            ISubscription subscription = SubscriptionFactory.Create(receiver, callback);

            _queue.Enqueue((false, subscription));
        }

        public void Unsubscribe<TMessage>(object receiver)
            where TMessage : class, IMessage
        {
            ISubscription subscription = SubscriptionFactory.Create<TMessage>(receiver, _ => Task.CompletedTask);

            _queue.Enqueue((true, subscription));
        }

        private Task<IReadOnlyList<ISubscription>> GetSubscriptions<TMessage>()
            where TMessage : IMessage
        {
            return Task.Run(() => {
                lock (_lock)
                {
                    while (_queue.TryDequeue(out (bool Remove, ISubscription Subscription) value))
                    {
                        if (value.Remove)
                        {
                            _subscriptions.Remove(value.Subscription);
                        }
                        else
                        {
                            _subscriptions.Add(value.Subscription);
                        }
                    }

                    return (IReadOnlyList<ISubscription>)_subscriptions.Where(s => s.MessageType == typeof(TMessage)).ToList();
                }
            });
        }

        private async Task SendMessageInternal<TMessage>(TMessage message)
            where TMessage : IMessage
        {
            IReadOnlyList<ISubscription> subscriptions = await this.GetSubscriptions<TMessage>();

            var taskList = new List<Task>(subscriptions.Count);

            foreach (ISubscription subscription in subscriptions)
            {
                taskList.Add(Task.Run(() => subscription.SendMessage(message)));
            }

            await Task.WhenAll(taskList);
        }
    }
}
