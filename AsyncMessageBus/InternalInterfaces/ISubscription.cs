﻿using System;
using System.Threading.Tasks;

namespace AsyncMessageBus.Interfaces
{
    internal interface ISubscription : IEquatable<ISubscription>
    {
        Type MessageType { get; }

        bool IsAlive { get; }

        Task SendMessage(IMessage message);

        bool HasReceiver(object receiver);
    }
}
