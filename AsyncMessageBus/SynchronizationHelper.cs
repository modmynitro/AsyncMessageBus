﻿using System;
using System.Threading.Tasks;

using AsyncMessageBus.Interfaces;

namespace AsyncMessageBus
{
    internal class SynchronizationHelper
    {
        private readonly ISynchronizer _synchronizer;

        public SynchronizationHelper(ISynchronizer synchronizer)
        {
            if (synchronizer is null)
                throw new ArgumentNullException(nameof(synchronizer));

            _synchronizer = synchronizer;
        }

        public Task Synchronize(Func<Task> func)
        {
            if (func is null)
                throw new ArgumentNullException(nameof(func));

            return this.Synchronize_INTERNAL(func);
        }

        private async Task Synchronize_INTERNAL(Func<Task> func)
        {
            if (_synchronizer.SynchronizationRequired)
            {
                await _synchronizer.SynchronizeAsync(async () => await func());
            }
            else
            {
                await func();
            }
        }
    }
}
