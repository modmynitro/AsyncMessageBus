﻿using System;
using System.Diagnostics;
using AsyncMessageBus.Interfaces;

namespace AsyncMessageBus
{
    internal sealed class DefaultExceptionHandler : IExceptionHandler
    {
        public void Handle(Exception exception)
        {
            Debug.WriteLine(exception);
        }

        public static IExceptionHandler ExceptionHandler { get; } = new DefaultExceptionHandler();
    }
}