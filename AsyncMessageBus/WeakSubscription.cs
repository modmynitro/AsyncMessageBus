﻿using System;
using System.Reflection;
using System.Threading.Tasks;

using AsyncMessageBus.Interfaces;

namespace AsyncMessageBus
{
    internal class WeakSubscription<TMessage> :
        ISubscription
        where TMessage : IMessage
    {
        private readonly WeakReference _weakTarget;
        private readonly MethodInfo _callback;

        public WeakSubscription(object target, Func<TMessage, Task> callback)
        {
            _weakTarget = new WeakReference(target);
            _callback = callback.Method;
        }

        public bool IsAlive => throw new NotImplementedException();
        public Type MessageType => typeof(TMessage);

        public bool HasReceiver(object receiver)
        {
            if (receiver is null)
                throw new ArgumentNullException(nameof(receiver));

            object? target = _weakTarget.Target;

            return target == receiver;
        }

        public Task SendMessage(IMessage message)
        {
            if (message is null)
                throw new ArgumentNullException(nameof(message));

            if (!(message is TMessage))
                throw new ArgumentException(nameof(message));

            MethodInfo callback = _callback;
            object? target = _weakTarget.Target;

            if (callback == null || target == null)
                return Task.CompletedTask;

            return (Task)callback.Invoke(target, new[] { message });
        }

        public bool Equals(ISubscription other)
        {
            if (other == null)
                return false;
            if (ReferenceEquals(this, other))
                return true;

            return MessageType == other.MessageType && other.HasReceiver(_weakTarget.Target);
        }
    }
}
