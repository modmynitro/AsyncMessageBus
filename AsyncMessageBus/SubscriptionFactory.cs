﻿using System;
using System.Threading.Tasks;

using AsyncMessageBus.Interfaces;

namespace AsyncMessageBus
{
    internal static class SubscriptionFactory
    {
        public static ISubscription Create<TMessage>(object recipient, Func<TMessage, Task> callback)
            where TMessage : IMessage
        {
            if (callback.Method.IsStatic)
                return new StrongSubscription<TMessage>(recipient, callback);

            return new WeakSubscription<TMessage>(recipient, callback);
        }
    }
}
