﻿using AsyncMessageBus.Interfaces;

namespace AsyncMessageBus
{
    public static class AsyncMessageBusFactory
    {
        /// <summary>
        /// Creates the message bus.
        /// </summary>
        /// <returns>The newly created message bus never <see langword="null"/>.</returns>
        public static IAsyncMessageBus Create()
        {
            return new AsyncMessageBus(null, null); ;
        }

        /// <summary>
        /// Creates the message bus.
        /// </summary>
        /// <param name="synchronizer">The synchronizer. May be <see langword="null"/>.</param>
        /// <param name="exceptionHandler">
        /// The exception handler which will be called if an exception is thrown while posting a
        /// message. May be <see langword="null"/>.
        /// </param>
        /// <returns>The newly created message bus never <see langword="null"/>.</returns>
        public static IAsyncMessageBus Create(ISynchronizer? synchronizer, IExceptionHandler? exceptionHandler)
        {
            return new AsyncMessageBus(synchronizer, exceptionHandler);
        }
    }
}